angular.module('starter.controllers', [])
.controller('LoginCtrl', ['$scope','$state', function($scope,$state) {

  $scope.login=function(){
    $state.go('tab.globalChallenges');
  };
}])

.controller('ChallengeCtrl',[
        '$scope','$ionicScrollDelegate', '$ionicSlideBoxDelegate', '$timeout', '$location', '$ionicModal', 'PostsService',
function($scope,  $ionicScrollDelegate, $ionicSlideBoxDelegate,  $timeout, $location, $ionicModal, PostsService){
  $scope.myChallenges = [];
  PostsService.getPosts().then(function(result){
      $scope.challenges = result;
      $scope.current = $scope.challenges[0];
  });
  $ionicModal.fromTemplateUrl('image-modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
    });

    $scope.openModal = function() {
      $scope.modal.show();
    };

    $scope.closeModal = function() {
      $scope.modal.hide();
    };

    //Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
      $scope.modal.remove();
    });
    // Execute action on hide modal
    $scope.$on('modal.hide', function() {
      // Execute action
    });
    // Execute action on remove modal
    $scope.$on('modal.removed', function() {
      // Execute action
    });
    $scope.$on('modal.shown', function() {
      console.log('Modal is shown!');
    });

    $scope.imageSrc = 'http://ionicframework.com/img/ionic-logo-blog.png';

    $scope.showImage = function(index) {
      $scope.imageSrc = 'img/accepted.png';
      $scope.openModal();
    };


  $scope.transitionLeft = function (index) {
    $scope.isSad = true;
    $timeout(function () {
      $scope.isSad = false;
    }, 600);
  };

  $scope.transitionRight = function (index) {
    $scope.myChallenges.push(index);
  };

  $scope.cardDestroyed = function (index) {
    $scope.challenges.splice(index, 1);
    $scope.current = $scope.challenges[0];
  };

  $scope.relaunchCards = function(){
    console.log('reload')
    PostsService.getPosts().then(function(result){
      console.log(result)
      $scope.challenges= result;
    });
  };

  $scope.go = function (page) {
    location.path(page);
  }

  var freezeTo = null;
  $scope.disableVerticalScrolling=function(){
    $ionicScrollDelegate.freezeScroll(true);

     freezeTo=$timeout(function(){
       if(freezeTo){
         $timeout.cancel(freezeTo);freezeTo=null;
       }

       $ionicScrollDelegate.freezeScroll(false);
       $timeout.cancel(freezeTo);
     },500);
  };
}])

/**
 * Challenge ctrl
 */

.controller('GlobalChallengeCtrl',[
        '$scope','$controller',
function($scope,  $controller){
  angular.extend(this, $controller('ChallengeCtrl', {$scope: $scope}));
}])


.controller('MyChallengeCtrl',[
        '$scope','$controller',
function($scope,  $controller){
  angular.extend(this, $controller('ChallengeCtrl', {$scope: $scope}));
}])

.controller('LastChallengesCtrl',[
        '$scope','$controller',
function($scope,  $controller){
  angular.extend(this, $controller('ChallengeCtrl', {$scope: $scope}));
}])


.controller('RankingCtrl',[
        '$scope','UsersService',
function($scope,  UsersService){
   UsersService.getUsers().then(function(result){
       $scope.top = result;
   });
}])

.controller('CharityCtrl',[
        '$scope', 'CharitysService',
function($scope,  CharitysService){
  CharitysService.getCharitys().then(function (result) {
    $scope.charitys = result;
  });

}])
