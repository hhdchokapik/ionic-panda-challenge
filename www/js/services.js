angular.module('starter.services', [])

.factory('ChallengesService', challengesService);

var challengesService = ['$scope', '$q', function ($scope, $q) {
  return function(){
    var posts = {
      id : 1,
      sender : {

      },
      challenger : {

      },
      state : '',
      challenge : {

      },
      pledge : {

      },
      gift : {

      },
      attachement : '',
      charity : {

      },

    };
    var datas = {
       challenges : [
         {
           id : 1,
           title : 'Covoiturage',
           icon : ''
         },
         {
           id : 2,
           title : 'Glaner chez l\'aggriculteur',
           icon : ''
         },
         {
           id : 3,
           title : 'Don\'t take your car to go to work',
           icon : ''
         },
         {
           id : 4,
           title : 'Rapporter des bouteilles',
           icon : ''
         },
         {
           id : 5,
           title : 'Composter des déchets',
           icon : ''
         },
         {
           id : 6,
           title : 'Aller à la déchèterie',
           icon : ''
         },
         {
           id : 7,
           title : 'Transport en commun',
           icon : ''
         },
         {
           id : 8,
           title : 'Un restau avec un sans abris',
           icon : ''
         },
         {
           id : 9,
           title : 'Signer une pétition',
           icon : ''
         },
         {
           id : 10,
           title : 'Recycle your furniture',
           sponsored : true,
           icon : ''
         },
         {
           id : 11,
           title : 'Clean the parking',
           sponsored : true,
           icon : ''
         }
      ],
      pledges : [
        {
          id : 1,
          title : 'Superman pants',
          icon : ''
        },
        {
          id : 2,
          title : 'Hilarious hat on',
          icon : ''
        },
        {
          id : 3,
          title : 'Sock on shoulder',
          icon : ''
        },
        {
          id : 4,
          title : 'Sing a child song',
          icon : ''
        },
        {
          id : 5,
          title : 'Wearing a swim slip',
          icon : ''
        },
        {
          id : 6,
          title : 'Dancing',
          icon : ''
        },
        {
          id : 7,
          title : 'Moonwalk',
          icon : ''
        },
        {
          id : 8,
          title : 'Cat makeup',
          icon : ''
        },
        {
          id : 9,
          title : 'Pirate costume',
          icon : ''
        },
        {
          id : 10,
          title : 'Diving palm',
          icon : ''
        },
        {
          id : 11,
          title : 'Diving mask and snorkel',
          icon : ''
        },
        {
          id : 12,
          title : 'Zombie mod',
          icon : ''
        },
      ],
      gifts : [
        {
          id : 1,
          title : '10% off on the all store'
        },
        {
          id : 2,
          title : 'Have a free meal'
        }
      ],
      charitys : [
        {
          id : 1,
          title : '',
          description : '',
          website : '',
          icon : ''
        },
        {
          id : 2,
          title : '',
          description : '',
          website : '',
          icon : ''
        },
        {
          id : 3,
          title : '',
          description : '',
          website : '',
          icon : ''
        },
        {
          id : 4,
          title : '',
          description : '',
          website : '',
          icon : ''
        },
        {
          id : 5,
          title : '',
          description : '',
          website : '',
          icon : ''
        }
      ],

      getChallenges : function(){
          return $q(function(res,rej){res(datas.challenges);});
      },
      getChallenge : function(id){
        return datas.challenges[id];
      },
      addChallenge : function(event){
        datas.challenges.add(event);
        return datas.challenges;
      },
      getPledges : function(){
          return $q(function(res,rej){res(datas.pleges);});
      },
      getPledge : function(id){
        return datas.pleges[id];
      },
      addPledge : function(event){
        datas.pleges.add(event);
        return datas.pleges;
      },
      getGifts : function(){
          return $q(function(res,rej){res(datas.gifts);});
      },
      getGift : function(id){
        return datas.gifts[id];
      },
      addGift : function(event){
        datas.gifts.add(event);
        return datas.gifts;
      },
      getCharitys : function(){
          return $q(function(res,rej){res(datas.charitys);});
      },
      getCharity : function(id){
        return datas.charitys[id];
      },
      addCharitys : function(event){
        datas.charitys.add(event);
        return datas.charitys;
      }
    };
    return datas;
  };
}];
