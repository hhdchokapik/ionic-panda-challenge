// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', [
                              'ngIOS9UIWebViewPatch',
                              'ionic.contrib.ui.tinderCards',

                              'ionic',
                              'starter.controllers',
                              'starter.services',
                              'starter.filters',
])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // Each tab has its own nav history stack:
/*
  .state('lastchallenges', {
    url: '/lastchallenges',
    templateUrl: 'templates/last-challenges.html',
    controller: 'LastChallengesCtrl'
  })

  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'LoginCtrl'
  })

  .state('register', {
    url: '/register',
    templateUrl: 'templates/register.html',
    //controller: 'RegisterCtrl'
  })
*/
  .state('make-gift', {
    url: '/make-gift',
    templateUrl: 'templates/make-gift.html',
    controller: 'CharityCtrl'
  })

  .state('tab', {
   url: '/tab',
   abstract: true,
   templateUrl: 'templates/tabs.html'
 })

 // Each tab has its own nav history stack:
 .state('tab.globalChallenges', {
   url: '/global-challenges',
   views: {
     'globalChallenges': {
       templateUrl: 'templates/global-challenges.html',
       controller:'GlobalChallengeCtrl'
     }
   }
 })

 .state('tab.myChallenges', {
   url: '/my-challenges',
   views: {
     'myChallenges': {
       templateUrl: 'templates/my-challenges.html',
       controller:'MyChallengeCtrl'
     }
   }
 })

 .state('tab.rankings', {
   url: '/rankings',
   views: {
     'rankings': {
       templateUrl: 'templates/rankings.html',
       controller : 'RankingCtrl'
     }
   }
 })


//myChallenges
//rankings
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/global-challenges');

});
