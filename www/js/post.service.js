angular.module('starter.services')
  .factory('PostsService', ['$q', function ($q) {
    var datas = {
      posts : [
        {
          id : 1,
          sender : {
            id : 3,
            "gender": "male",
            "name": {
                "title": "mr",
                "first": "taro",
                "last": "werkman"
            }
          },
          challenger : {
            "gender": "female",
            "name": {
                "title": "mrs",
                "first": "inaya",
                "last": "dumas"
            },
          },
          state : 'Pledged',
          title : 'Sign & Swim',
          description : 'Sign a petition with your swaggest swin suit',
          challenge : {
            id : 1,
            title : 'Sign a petition',
            icon : 'plume'
          },
          pledge : {
            id : 1,
            title : 'Swim suit',
            icon : 'slip'
          },
          gift : {

          },
          attachement : '',
          charity : {
            name : '7 Lieux',
            website : 'www.7lieux.com',
            description : 'Le mieux vivre au naturel, une association qui cherche, trouve et anime tous les secteurs de l\'écologie au quotidien',
            icon : 'img/Greenpeace-logo.svg'
          }
        },
        {
          id : 1,
          title : 'The Scuba sorting',
          description : 'Sort your wastes while you ware Diving palms',
          sender : {
            id : 17,
            "gender": "male",
            "name": {
              "title": "IKEA",
              "first": "IKEA",
              "last": "IKEA"
            },
            icon : ''
          },
          challenger : {
            "gender": "female",
            "name": {
                "title": "mrs",
                "first": "inaya",
                "last": "dumas"
            },
          },
          state : 'Pledged',
          challenge : {
            id : 1,
            title : 'Sorting Waste',
            icon : 'pomme'
          },
          pledge : {
            id : 1,
            title : 'Diving palm',
            icon : 'palme'
          },
          gift : {
            id : 1,
            title : '10% off on the all store'
          },
          attachement : '',
          charity : {
            name : '',
            website : 'www.7lieux.com',
            description : 'Le mieux vivre au naturel, une association qui cherche, trouve et anime tous les secteurs de l\'écologie au quotidien',
            icon : 'img/WWF.svg'
          }
        },
        {
          title : 'B’Twin of the Caribbean',
          description : 'Buy a B’Twin by DECATHLON dressed, like an evil of the seven seas ! and try to win a treasure*',
          legal : '*lorem ipsum dolor sit amet et consectur',
          id : 1,
          sender : {
            id : 16,
            "gender": "sponsor",
            "name": {
              "title": "Decatlhon",
              "first": "Decatlhon",
              "last": "Decatlhon"
            },
            icon : 'img/decathlon.jpg'
          },
          challenger : {
            id : 1,
            "gender": "female",
            "name": {
                "title": "mrs",
                "first": "inaya",
                "last": "dumas"
            },
          },
          state : 'Pledged',
          challenge : {
            id : 11,
            title : 'Buy a bitwin bike',
            sponsored : true,
            icon : 'velo'
          },
          pledge : {
            id : 2,
            title : 'Pirate costume',
            icon : 'pirate'
          },
          gift : {
            id : 2,
            title : '10 % off'
          },
          attachement : '',
          charity : {

          }
        },
/*
        {
          id : 1,
          sender : {
            id : 3,
            "gender": "male",
            "name": {
                "title": "mr",
                "first": "taro",
                "last": "werkman"
            }
          },
          challenger : {
            "gender": "female",
            "name": {
                "title": "mrs",
                "first": "inaya",
                "last": "dumas"
            },
          },
          state : 'Pledged',
          challenge : {
            id : 1,
            title : 'Covoiturage',
            icon : ''
          },
          pledge : {
            id : 1,
            title : 'Superman pants',
            icon : ''
          },
          gift : {

          },
          attachement : '',
          charity : {

          }
        },
        {
          id : 1,
          sender : {
            id : 3,
            "gender": "male",
            "name": {
                "title": "mr",
                "first": "taro",
                "last": "werkman"
            }
          },
          challenger : {
            "gender": "female",
            "name": {
                "title": "mrs",
                "first": "inaya",
                "last": "dumas"
            },
          },
          state : 'Pledged',
          challenge : {
            id : 1,
            title : 'Covoiturage',
            icon : ''
          },
          pledge : {
            id : 1,
            title : 'Superman pants',
            icon : ''
          },
          gift : {

          },
          attachement : '',
          charity : {

          }
        },
        {
          id : 1,
          sender : {
            id : 3,
            "gender": "male",
            "name": {
                "title": "mr",
                "first": "taro",
                "last": "werkman"
            }
          },
          challenger : {
            "gender": "female",
            "name": {
                "title": "mrs",
                "first": "inaya",
                "last": "dumas"
            },
          },
          state : 'Pledged',
          challenge : {
            id : 1,
            title : 'Covoiturage',
            icon : ''
          },
          pledge : {
            id : 1,
            title : 'Superman pants',
            icon : ''
          },
          gift : {

          },
          attachement : '',
          charity : {

          }
        }*/
      ]
    };
    return {
      getPosts : function(){
          return new $q(function(res,rej){
            var newData = _.clone(datas,true);
            res(newData.posts);
          });
      },
      getPost : function(id){
        return datas.posts[id];
      },
      addPost : function(event){
        datas.posts.add(event);
        return datas.posts;
      }
    };
  }]);
