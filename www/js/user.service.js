angular.module('starter.services')
  .factory('UsersService', ['$q', function ($q) {
    var datas = {
      users : [
        {
          id : 1,
          "gender": "female",
          "name": {
              "title": "mrs",
              "first": "inaya",
              "last": "dumas"
          },
          "location": {
              "street": "1442 rue denfert-rochereau",
              "city": "vitry-sur-seine",
              "state": "nièvre",
              "zip": 65144
          },
          "email": "inaya.dumas@example.com",
          "username": "crazylion159",
          "password": "blessing",
          "salt": "T9rZFshF",
          "md5": "b63b3943438c1f64ff7e323443a9ac25",
          "sha1": "ca0c09d9b3c9fc0f09fb3effbd2530fb5708e449",
          "sha256": "833208b7426c7a1cd52ae0627fa15d2abfbff64af4e1a01cabae9b8e46500a37",
          "registered": 1212263077,
          "dob": 803074653,
          "phone": "01-15-09-00-53",
          "cell": "06-56-54-52-96",
          "INSEE": "2950610131970 95",
          "picture": {
              "large": "https://randomuser.me/api/portraits/women/69.jpg",
              "medium": "https://randomuser.me/api/portraits/med/women/69.jpg",
              "thumbnail": "https://randomuser.me/api/portraits/thumb/women/69.jpg"
          }
        },
        {
          id : 2,
          "gender": "female",
          "name": {
              "title": "ms",
              "first": "méline",
              "last": "clement"
          },
          "location": {
              "street": "6194 rue jean-baldassini",
              "city": "versailles",
              "state": "cher",
              "zip": 30270
          },
          "email": "méline.clement@example.com",
          "username": "heavypeacock177",
          "password": "peekaboo",
          "salt": "RNvfgWn2",
          "md5": "c0777dbcaa1309feabe2fd748493aad7",
          "sha1": "e48939c5d2bec9de03fb5c73bdf21b0268219666",
          "sha256": "759f659a0607881edb87be1246b15e6f165195dbc5bf4b9601e6498bb22e0775",
          "registered": 1427420217,
          "dob": 301409422,
          "phone": "02-67-70-68-06",
          "cell": "06-98-58-21-27",
          "INSEE": "2790759810938 27",
          "picture": {
              "large": "https://randomuser.me/api/portraits/women/93.jpg",
              "medium": "https://randomuser.me/api/portraits/med/women/93.jpg",
              "thumbnail": "https://randomuser.me/api/portraits/thumb/women/93.jpg"
          }
        },
        {
          id : 3,
          "gender": "male",
          "name": {
              "title": "mr",
              "first": "taro",
              "last": "werkman"
          },
          "location": {
              "street": "6765 domplein",
              "city": "sluis",
              "state": "utrecht",
              "zip": 21525
          },
          "email": "taro.werkman@example.com",
          "username": "beautifulleopard916",
          "password": "rockies",
          "salt": "qZWGoWpI",
          "md5": "d59952eb82a942583b8a3a9b3f46aa79",
          "sha1": "752b1ce8215bcbdc2a1eeaf3703b79f576413ff1",
          "sha256": "08c02778fa47d1bb3b5a6e347713e6016daefcdb28eb2a21a5d730e3e550100a",
          "registered": 950037532,
          "dob": 345336543,
          "phone": "(811)-755-5758",
          "cell": "(655)-328-1666",
          "BSN": "34444279",
          "picture": {
              "large": "https://randomuser.me/api/portraits/men/60.jpg",
              "medium": "https://randomuser.me/api/portraits/med/men/60.jpg",
              "thumbnail": "https://randomuser.me/api/portraits/thumb/men/60.jpg"
          }
        },
        {
          id : 4,
          "gender": "male",
          "name": {
              "title": "mr",
              "first": "angel",
              "last": "benitez"
          },
          "location": {
              "street": "7356 calle de atocha",
              "city": "toledo",
              "state": "extremadura",
              "zip": 31922
          },
          "email": "angel.benitez@example.com",
          "username": "redswan862",
          "password": "rolling",
          "salt": "0HqiDfhc",
          "md5": "dfc3909f8b2497d4d1c4a0326801dbe1",
          "sha1": "f0af528f85f9ba06b34bf2a4dbfadb9cb0272c06",
          "sha256": "29e79f8058d942029adb92116b06bfe63ce023b814d98829c42ae87a87d85d79",
          "registered": 1358227671,
          "dob": 719711924,
          "phone": "957-460-670",
          "cell": "687-145-555",
          "DNI": "28390815-A",
          "picture": {
              "large": "https://randomuser.me/api/portraits/men/40.jpg",
              "medium": "https://randomuser.me/api/portraits/med/men/40.jpg",
              "thumbnail": "https://randomuser.me/api/portraits/thumb/men/40.jpg"
          }
        },
        {
          id : 5,
          "gender": "female",
          "name": {
              "title": "miss",
              "first": "andrea",
              "last": "rice"
          },
          "location": {
              "street": "9435 victoria street",
              "city": "norwich",
              "state": "south glamorgan",
              "zip": "OW5G 9JU"
          },
          "email": "andrea.rice@example.com",
          "username": "crazylion462",
          "password": "pelican",
          "salt": "MgtACqZl",
          "md5": "1ee5ef7c6a088795c5ce06959868787f",
          "sha1": "0b617b68d798b838e7e3a9f454aa1ec43d29101b",
          "sha256": "560d5046c62b773412c137a016947ccc564deb264bf8c51bbd2d3adfffc23ec6",
          "registered": 935842709,
          "dob": 401098511,
          "phone": "017683 36964",
          "cell": "0786-089-523",
          "NINO": "LL 01 94 08 R",
          "picture": {
              "large": "https://randomuser.me/api/portraits/women/18.jpg",
              "medium": "https://randomuser.me/api/portraits/med/women/18.jpg",
              "thumbnail": "https://randomuser.me/api/portraits/thumb/women/18.jpg"
          }
        },
        {
          id : 6,
          "gender": "male",
          "name": {
              "title": "mr",
              "first": "stephen",
              "last": "ellis"
          },
          "location": {
              "street": "9895 rookery road",
              "city": "Athy",
              "state": "colorado",
              "zip": 64649
          },
          "email": "stephen.ellis@example.com",
          "username": "ticklishtiger603",
          "password": "digger",
          "salt": "GoIB5Ivj",
          "md5": "f6170773dd35b77b832cd64bd1036794",
          "sha1": "b390f4ffa1391f16683e5e3988b190c23b47ed0f",
          "sha256": "4bd3307c21f42c4eeff79d1e287c28f1350c5185d29854866d58142abefeb80d",
          "registered": 1345001504,
          "dob": 1192606596,
          "phone": "051-946-0491",
          "cell": "081-592-0004",
          "PPS": "8094266T",
          "picture": {
              "large": "https://randomuser.me/api/portraits/men/72.jpg",
              "medium": "https://randomuser.me/api/portraits/med/men/72.jpg",
              "thumbnail": "https://randomuser.me/api/portraits/thumb/men/72.jpg"
          }
        },
        {
          id : 7,
          "gender": "female",
          "name": {
            "title": "ms",
            "first": "linnea",
            "last": "heikkinen"
          },
          "location": {
            "street": "5385 siilitie",
            "city": "vårdö",
            "state": "central finland",
            "zip": 84397
          },
          "email": "linnea.heikkinen@example.com",
          "username": "tinyfish862",
          "password": "excess",
          "salt": "xwK9G4LN",
          "md5": "6a7cb4e61dcd9e85d8d0634ccaa8d3f1",
          "sha1": "e8dcd56d1f748844fd8b217b5cfa611e68f110b1",
          "sha256": "800aeb56f962a0da3aef386d0cceb6dd77ce67699e0aba21c2bfad70a631b1e4",
          "registered": 1273658215,
          "dob": 1340792290,
          "phone": "06-381-521",
          "cell": "045-448-58-69",
          "HETU": "270612A908M",
          "picture": {
            "large": "https://randomuser.me/api/portraits/women/41.jpg",
            "medium": "https://randomuser.me/api/portraits/med/women/41.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/women/41.jpg"
          }
        },
        {
          id : 8,
          "gender": "female",
          "name": {
            "title": "ms",
            "first": "chloé",
            "last": "renaud"
          },
          "location": {
            "street": "6795 rue courbet",
            "city": "angers",
            "state": "eure-et-loir",
            "zip": 23267
          },
          "email": "chloé.renaud@example.com",
          "username": "smalllion832",
          "password": "ccbill",
          "salt": "EMvsUDhk",
          "md5": "9b78627673524ac6630f63aaee601a9c",
          "sha1": "8cf4b411ad035b5a64a42a8f1284852a8b6f189a",
          "sha256": "b7f96830f8d9455ab8bf7c349680532790e3b6166295e0ecb378d3fc76725d30",
          "registered": 1126944878,
          "dob": 216018745,
          "phone": "04-76-94-87-08",
          "cell": "06-45-29-30-62",
          "INSEE": "2761143281087 95",
          "picture": {
            "large": "https://randomuser.me/api/portraits/women/77.jpg",
            "medium": "https://randomuser.me/api/portraits/med/women/77.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/women/77.jpg"
          }
        },
        {
          id : 9,
          "gender": "male",
          "name": {
            "title": "mr",
            "first": "sohan",
            "last": "lambert"
          },
          "location": {
            "street": "7291 rue barrier",
            "city": "argenteuil",
            "state": "la réunion",
            "zip": 92473
          },
          "email": "sohan.lambert@example.com",
          "username": "bluemeercat591",
          "password": "discover",
          "salt": "w0UQtWIT",
          "md5": "2a2a6afa6fa9945cc96a44ca717e9b01",
          "sha1": "32a703adf5e5938030fc928ae242f3c8a54f775f",
          "sha256": "5d3331050a564c31781bc675944c080313e004a8b4f6fccef54c3697a1da150a",
          "registered": 1082719412,
          "dob": 203942389,
          "phone": "01-94-46-30-04",
          "cell": "06-89-04-26-17",
          "INSEE": "1760615992564 20",
          "picture": {
            "large": "https://randomuser.me/api/portraits/men/1.jpg",
            "medium": "https://randomuser.me/api/portraits/med/men/1.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/men/1.jpg"
          }
        },
        {
          id : 10,
          "gender": "male",
          "name": {
            "title": "mr",
            "first": "gary",
            "last": "allen"
          },
          "location": {
            "street": "5940 depaul dr",
            "city": "sunshine coast",
            "state": "queensland",
            "zip": 21419
          },
          "email": "gary.allen@example.com",
          "username": "ticklishbutterfly706",
          "password": "darryl",
          "salt": "mbsWjS2n",
          "md5": "534fed2985030833a57a73df704de8d9",
          "sha1": "9e9baaa182564b5b34240b87053e6aefdf79a45c",
          "sha256": "a2c12102c986bd88fae1405ab56f5389a892358b2963c72c0759f2abd4500271",
          "registered": 1266503555,
          "dob": 449862323,
          "phone": "01-9137-9489",
          "cell": "0479-634-897",
          "TFN": "761120984",
          "picture": {
            "large": "https://randomuser.me/api/portraits/men/71.jpg",
            "medium": "https://randomuser.me/api/portraits/med/men/71.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/men/71.jpg"
          }
        },
        {
          id : 11,
          "gender": "male",
          "name": {
            "title": "mr",
            "first": "patrick",
            "last": "morales"
          },
          "location": {
            "street": "1426 shady ln dr",
            "city": "queanbeyan",
            "state": "new south wales",
            "zip": 28412
          },
          "email": "patrick.morales@example.com",
          "username": "silverbird861",
          "password": "veronika",
          "salt": "nQH8Tb65",
          "md5": "f41ae6375f437ddb9506574eb0601ed6",
          "sha1": "585feec08ae2b8d24c6504ae3ea1c3e038673032",
          "sha256": "0874c1e91b3794e1f6f4b84e6e152e3eb25c848f48849f2c50b984fcb9466805",
          "registered": 1344632843,
          "dob": 1204428103,
          "phone": "02-9976-9801",
          "cell": "0439-440-373",
          "TFN": "802949650",
          "picture": {
            "large": "https://randomuser.me/api/portraits/men/70.jpg",
            "medium": "https://randomuser.me/api/portraits/med/men/70.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/men/70.jpg"
          }
        },
        {
          id : 12,
          "gender": "female",
          "name": {
            "title": "mrs",
            "first": "lucia",
            "last": "navarro"
          },
          "location": {
            "street": "8452 calle de segovia",
            "city": "gijón",
            "state": "canarias",
            "zip": 49732
          },
          "email": "lucia.navarro@example.com",
          "username": "ticklishlion430",
          "password": "bmw325",
          "salt": "6zfznmgF",
          "md5": "188d5b4abcbcf21416090bd27cb8d3f9",
          "sha1": "7742c39180a2c07ba9c347d5c0169574776236d8",
          "sha256": "7538456c4a0724cc6c09fa0e06be25a54d013a25430658d2c412b46ffd7d9324",
          "registered": 1039833858,
          "dob": 1096569317,
          "phone": "913-110-846",
          "cell": "669-279-945",
          "DNI": "46801495-W",
          "picture": {
            "large": "https://randomuser.me/api/portraits/women/7.jpg",
            "medium": "https://randomuser.me/api/portraits/med/women/7.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/women/7.jpg"
          }
        },
        {
          id : 13,
          "gender": "male",
          "name": {
            "title": "mr",
            "first": "lino",
            "last": "renard"
          },
          "location": {
            "street": "9306 place de l'abbé-georges-hénocque",
            "city": "orléans",
            "state": "pyrénées-atlantiques",
            "zip": 41219
          },
          "email": "lino.renard@example.com",
          "username": "orangewolf194",
          "password": "speaker",
          "salt": "Auyqub6X",
          "md5": "2c4e73ddae37bd9373809a51ff85101d",
          "sha1": "91ca5a47e77572d5c05c7468410070f528449b27",
          "sha256": "aeab9d5e9d96b5ca6aa950461cdf16aca2c11def4a11364144ffef6d64a618de",
          "registered": 1384823999,
          "dob": 407144222,
          "phone": "01-06-72-96-50",
          "cell": "06-74-04-56-09",
          "INSEE": "1821131226091 74",
          "picture": {
            "large": "https://randomuser.me/api/portraits/men/19.jpg",
            "medium": "https://randomuser.me/api/portraits/med/men/19.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/men/19.jpg"
          }
        },
        {
          id : 14,
          "gender": "male",
          "name": {
            "title": "mr",
            "first": "max",
            "last": "ward"
          },
          "location": {
            "street": "4378 park avenue",
            "city": "city of london",
            "state": "mid glamorgan",
            "zip": "A2 5XE"
          },
          "email": "max.ward@example.com",
          "username": "biglion847",
          "password": "sancho",
          "salt": "m6OES516",
          "md5": "80454a7290c83bf62a85862b3868600a",
          "sha1": "66dafaa36647bbb7c43655d60472891fcb82b37a",
          "sha256": "96426bd9c1e68f844274bef1dcf61123e1c3af4758199f44c6c7319c425c701a",
          "registered": 1339236477,
          "dob": 550478128,
          "phone": "015242 81640",
          "cell": "0760-997-499",
          "NINO": "JZ 00 38 65 X",
          "picture": {
            "large": "https://randomuser.me/api/portraits/men/14.jpg",
            "medium": "https://randomuser.me/api/portraits/med/men/14.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/men/14.jpg"
          }
        },
        {
          id : 15,
          "gender": "male",
          "name": {
            "title": "mr",
            "first": "timeo",
            "last": "robert"
          },
          "location": {
            "street": "7976 rue pasteur",
            "city": "lille",
            "state": "isère",
            "zip": 11813
          },
          "email": "timeo.robert@example.com",
          "username": "whitefish613",
          "password": "ginger1",
          "salt": "1xf2aSDX",
          "md5": "8b8afb0e3660b5fcf1a8c82bbcf68c97",
          "sha1": "db6d5732b074573518491262117308e1c25221f8",
          "sha256": "d8bc573fc5e6c9104391ce3d93eda4923b9cc246553dca0371570fee4c034df3",
          "registered": 1352918897,
          "dob": 655073744,
          "phone": "05-66-78-35-26",
          "cell": "06-37-15-26-53",
          "INSEE": "1901042234961 54",
          "picture": {
            "large": "https://randomuser.me/api/portraits/men/7.jpg",
            "medium": "https://randomuser.me/api/portraits/med/men/7.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/men/7.jpg"
          }
        },
        {
          id : 16,
          "gender": "sponsor",
          "name": {
            "title": "Decatlhon",
            "first": "Decatlhon",
            "last": "Decatlhon"
          },
          "location": {
            "street": "7976 rue pasteur",
            "city": "lille",
            "state": "isère",
            "zip": 11813
          },
          "email": "timeo.robert@example.com",
          "username": "whitefish613",
          "password": "ginger1",
          "salt": "1xf2aSDX",
          "md5": "8b8afb0e3660b5fcf1a8c82bbcf68c97",
          "sha1": "db6d5732b074573518491262117308e1c25221f8",
          "sha256": "d8bc573fc5e6c9104391ce3d93eda4923b9cc246553dca0371570fee4c034df3",
          "registered": 1352918897,
          "dob": 655073744,
          "phone": "05-66-78-35-26",
          "cell": "06-37-15-26-53",
          "INSEE": "1901042234961 54",
          "picture": {
            "large": "https://randomuser.me/api/portraits/men/7.jpg",
            "medium": "https://randomuser.me/api/portraits/med/men/7.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/men/7.jpg"
          }
        },
        {
          id : 17,
          "gender": "sponsor",
          "name": {
            "title": "IKEA",
            "first": "IKEA",
            "last": "IKEA"
          },
          "location": {
            "street": "7976 rue pasteur",
            "city": "lille",
            "state": "isère",
            "zip": 11813
          },
          "email": "timeo.robert@example.com",
          "username": "whitefish613",
          "password": "ginger1",
          "salt": "1xf2aSDX",
          "md5": "8b8afb0e3660b5fcf1a8c82bbcf68c97",
          "sha1": "db6d5732b074573518491262117308e1c25221f8",
          "sha256": "d8bc573fc5e6c9104391ce3d93eda4923b9cc246553dca0371570fee4c034df3",
          "registered": 1352918897,
          "dob": 655073744,
          "phone": "05-66-78-35-26",
          "cell": "06-37-15-26-53",
          "INSEE": "1901042234961 54",
          "picture": {
            "large": "https://randomuser.me/api/portraits/men/7.jpg",
            "medium": "https://randomuser.me/api/portraits/med/men/7.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/men/7.jpg"
          }
        },
        {
          id: 18,
          "gender": "sponsor",
          "name": {
            "title": "Orange",
            "first": "Orange",
            "last": "Orange"
          },
          "location": {
            "street": "7976 rue pasteur",
            "city": "lille",
            "state": "isère",
            "zip": 11813
          },
          "email": "timeo.robert@example.com",
          "username": "whitefish613",
          "password": "ginger1",
          "salt": "1xf2aSDX",
          "md5": "8b8afb0e3660b5fcf1a8c82bbcf68c97",
          "sha1": "db6d5732b074573518491262117308e1c25221f8",
          "sha256": "d8bc573fc5e6c9104391ce3d93eda4923b9cc246553dca0371570fee4c034df3",
          "registered": 1352918897,
          "dob": 655073744,
          "phone": "05-66-78-35-26",
          "cell": "06-37-15-26-53",
          "INSEE": "1901042234961 54",
          "picture": {
            "large": "https://randomuser.me/api/portraits/men/7.jpg",
            "medium": "https://randomuser.me/api/portraits/med/men/7.jpg",
            "thumbnail": "https://randomuser.me/api/portraits/thumb/men/7.jpg"
          }
        }
      ],
      getUsers : function(){
          return $q(function(res,rej){res(datas.users);});
      },
      getUser : function(id){
        return datas.users[id];
      },
      add : function(event){
        datas.users.add(event);
        return datas.users;
      }
    };
    return datas;
  }]);
