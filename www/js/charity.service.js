angular.module('starter.services')
  .factory('CharitysService', ['$q', function ($q) {
    var datas = {
      charitys : [
        {
          id : 1,
          title : 'WWF',
          description : 'Le Fonds mondial pour la nature, en anglais World Wide Fund, initialement le World Wildlife Fund créé en 1961, rebaptisé en 1986 World Wide Fund for Nature puis simplement WWF en 2003.',
          website : 'wwf.fr',
          icon : '../img/wwf.svg'
        },
        {
          id : 2,
          title : 'GreenPeace',
          description : 'Greenpeace is known for its direct actions and has been described as the most visible environmental organization in the world. Greenpeace has raised environmental issues to public knowledge, and influenced both the private and the public sector.',
          website : 'http://www.greenpeace.org/',
          icon : '../img/Greenpeace-logo.svg'
        },
        /*{
          id : 3,
          title : '',
          description : '',
          website : '',
          icon : ''
        },
        {
          id : 4,
          title : '',
          description : '',
          website : '',
          icon : ''
        },
        {
          id : 5,
          title : '',
          description : '',
          website : '',
          icon : ''
        }*/
      ]
    };
    return {
      getCharitys : function(){
          return new $q(function(res,rej){
            var newData = _.clone(datas,true);
            res(newData.charitys);
          });
      },
      getCharity : function(id){
        return datas.charitys[id];
      },
      addCharity : function(event){
        datas.charitys.add(event);
        return datas.charitys;
      }
    };
  }]);
